$(document).ready(function () {

  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false
  });

  $('.s-home-seo').on('click', function (e) {
    e.preventDefault();

    $(this).toggleClass('s-home-seo_active');
  });

  $('.s-header__toggle').on('click', function (e) {
    e.preventDefault();

    $('.s-header__center').slideToggle('fast');
    $('.s-header__right').slideToggle('fast');
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();

    $('.s-modal').toggle();
  });

  $('.s-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 's-modal__centered') {
      $('.s-modal').hide();
    }
  });

  $('.s-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.s-modal').hide();
  });

  $('.s-tabs__header li').click(function () {
    var tab_id = $(this).attr('data-tab');

    $('.s-tabs__header li').removeClass('current');
    $('.s-tabs__content').removeClass('current');

    $(this).addClass('current');
    $('#' + tab_id).addClass('current');
  });

  new Swiper('.s-top', {
    navigation: {
      nextEl: '.s-top .swiper-button-next',
      prevEl: '.s-top .swiper-button-prev',
    },
    pagination: {
      el: '.s-top .swiper-pagination',
      clickable: true,
    },
    on: {
      init: function () {
        $('.s-top__total').html(`0${this.slides.length}`);
        $('.s-top__current').html(`0${++this.realIndex}`);
      },
      slideChange: function () {
        $('.s-top__current').html(`0${++this.realIndex}`);
      },
    }
  });

  new Swiper('.s-home-projects__cards', {
    loop: true,
    navigation: {
      nextEl: '.s-home-projects .swiper-button-next',
      prevEl: '.s-home-projects .swiper-button-prev',
    },
    pagination: {
      el: '.s-home-projects .swiper-pagination',
      clickable: true,
    },
    spaceBetween: 100,
  });

  new Swiper('.s-home-reviews__cards', {
    loop: true,
    navigation: {
      nextEl: '.s-home-reviews .swiper-button-next',
      prevEl: '.s-home-reviews .swiper-button-prev',
    },
    pagination: {
      el: '.s-home-reviews .swiper-pagination',
      clickable: true,
    },
    spaceBetween: 100,
  });

  new Swiper('.s-papers__cards', {
    navigation: {
      nextEl: '.s-papers__cards .swiper-button-next',
      prevEl: '.s-papers__cards .swiper-button-prev',
    },
    pagination: {
      el: '.s-papers__cards .swiper-pagination',
      clickable: true,
    },
    spaceBetween: 40,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
      },
      1200: {
        slidesPerView: 4,
      },
    }
  });










  new Swiper('.w-welcome', {
    pagination: {
      el: '.w-welcome .swiper-pagination',
      clickable: true,
    }, autoplay: {
      delay: 5000,
    },
  });

  new Swiper('#woman .w-cards__cards', {
    navigation: {
      nextEl: '#woman .swiper-button-next',
      prevEl: '#woman .swiper-button-prev',
    },
    pagination: {
      el: '.w-good__images .swiper-pagination',
      clickable: true,
    },
    spaceBetween: 40,
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 3,
      },
      1200: {
        slidesPerView: 4,
      },
    }
  });

  new Swiper('.w-good__images', {
    navigation: {
      nextEl: '.w-good__images .swiper-button-next',
      prevEl: '.w-good__images .swiper-button-prev',
    },
    pagination: {
      el: '.w-good__images .swiper-pagination',
      clickable: true,
    },
    breakpoints: {
      1200: {
        slidesPerView: 2,
      },
    }
  });
});